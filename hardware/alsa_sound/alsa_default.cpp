
/* alsa_default.cpp
 **
 ** Copyright 2009 Wind River Systems
 **
 ** Licensed under the Apache License, Version 2.0 (the "License");
 ** you may not use this file except in compliance with the License.
 ** You may obtain a copy of the License at
 **
 **     http://www.apache.org/licenses/LICENSE-2.0
 **
 ** Unless required by applicable law or agreed to in writing, software
 ** distributed under the License is distributed on an "AS IS" BASIS,
 ** WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 ** See the License for the specific language governing permissions and
 ** limitations under the License.
 */
#define LOG_NDEBUG 0
//#define ALOGD
#define LOG_TAG "ALSAModule"
#include <utils/Log.h>

#include "AudioHardwareALSA.h"
#include <media/AudioRecord.h>

#include <cutils/properties.h>

#undef DISABLE_HARWARE_RESAMPLING

#define ALSA_NAME_MAX 128

#define ALSA_STRCAT(x,y) \
    if (strlen(x) + strlen(y) < ALSA_NAME_MAX) \
        strcat(x, y);

#ifndef ALSA_DEFAULT_SAMPLE_RATE
#define ALSA_DEFAULT_SAMPLE_RATE 44100 // in Hz
#endif

pthread_mutex_t lock;

int usb_playback_card = -1;
int usb_playback_device = -1;

int usb_capture_card = -1;
int usb_capture_device = -1;




namespace android_audio_legacy
{

static int s_device_open(const hw_module_t*, const char*, hw_device_t**);
static int s_device_close(hw_device_t*);
static status_t s_init(alsa_device_t *, ALSAHandleList &);
static status_t s_open(alsa_handle_t *, uint32_t, int, int flag=HW_PARAMS_FLAG_LPCM);
static status_t s_close(alsa_handle_t *);
static status_t s_close_l(alsa_handle_t *);
static status_t s_set_usb_device(int, uint32_t, uint32_t);
static status_t s_standby(alsa_handle_t *);
static status_t s_route(alsa_handle_t *, uint32_t, int, int flag=HW_PARAMS_FLAG_LPCM);

static hw_module_methods_t s_module_methods = {
    open            : s_device_open
};

extern "C" hw_module_t HAL_MODULE_INFO_SYM = {
    tag             : HARDWARE_MODULE_TAG,
    version_major   : 1,
    version_minor   : 0,
    id              : ALSA_HARDWARE_MODULE_ID,
    name            : "ALSA module",
    author          : "Wind River",
    methods         : &s_module_methods,
    dso             : 0,
    reserved        : { 0, },
};

static int s_device_open(const hw_module_t* module, const char* name,
        hw_device_t** device)
{
    alsa_device_t *dev;
    dev = (alsa_device_t *) malloc(sizeof(*dev));
    if (!dev) return -ENOMEM;

    memset(dev, 0, sizeof(*dev));

    /* initialize the procs */
    dev->common.tag = HARDWARE_DEVICE_TAG;
    dev->common.version = 0;
    dev->common.module = (hw_module_t *) module;
    dev->common.close = s_device_close;
    dev->init = s_init;
    dev->open = s_open;
    dev->close = s_close;
	dev->standby = s_standby;
    dev->route = s_route;
    dev->set_usb_device = s_set_usb_device;
	
    *device = &dev->common;
    return 0;
}

static int s_device_close(hw_device_t* device)
{
    free(device);
    return 0;
}

// ----------------------------------------------------------------------------

static const int DEFAULT_SAMPLE_RATE = ALSA_DEFAULT_SAMPLE_RATE;

static const char *devicePrefix[SND_PCM_STREAM_LAST + 1] = {
        /* SND_PCM_STREAM_PLAYBACK : */"AndroidPlayback",
        /* SND_PCM_STREAM_CAPTURE  : */"AndroidCapture",
};

#if 1
static void dumpHandleInfo(alsa_handle_t *handle){
    LOGD("\nDumpHandleInfo:\n");
    LOGD("devices: 0x%x\n", handle->devices);
    LOGD("format: 0x%x\n", handle->format);
    LOGD("channels: %d\n", handle->channels);
    LOGD("sampleRate: %d\n", handle->sampleRate);
    LOGD("latency: %d\n", handle->latency);
	LOGD("periodSize:%d\n", handle->periodSize);
    LOGD("bufferSize:%d\n", handle->bufferSize);

}
#else
static void dumpHandleInfo(alsa_handle_t *handle){}
#endif
#define DEFAULT_OUT_LATENCY     139319
#define DEFAULT_OUT_BUFFERSIZE  6144
#define DEFAULT_OUT_PERIODSIZE	1024
#define DEFAULT_OUT_PERIODS		3

#define DEFAULT_IN_LATENCY      139319
#define DEFAULT_IN_BUFFERSIZE   3072
#define DEFAULT_IN_PERIODSIZE	1024
#define DEFAULT_IN_PERIODS		3

static alsa_handle_t _Ext_Out = {
    module      : 0,
    devices     : AudioSystem::DEVICE_OUT_ALL,
    curDev      : 0,
    curMode     : 0,
    handle      : 0,
    format      : SND_PCM_FORMAT_S16_LE, // AudioSystem::PCM_16_BIT
    channels    : 2,
    sampleRate  : DEFAULT_SAMPLE_RATE,
    latency     : DEFAULT_OUT_LATENCY, // Desired Delay in usec  2048 * 3 /44100 = 139.311ms
	periodSize  : DEFAULT_OUT_PERIODSIZE,// size = 2048 * 16/8 * 2 = 8192 byte
    bufferSize  : DEFAULT_OUT_BUFFERSIZE,//2048*3  periods = 3  size = 8192 * 3 = 24 K byte
	periods     : DEFAULT_OUT_PERIODS,
    modPrivate  : 0,
    next		: 0,
};

static alsa_handle_t _defaultsOut = {
    module      : 0,
    devices     : AudioSystem::DEVICE_OUT_ALL,
    curDev      : 0,
    curMode     : 0,
    handle      : 0,
    format      : SND_PCM_FORMAT_S16_LE, // AudioSystem::PCM_16_BIT
    channels    : 2,
    sampleRate  : DEFAULT_SAMPLE_RATE,
    latency     : DEFAULT_OUT_LATENCY, // Desired Delay in usec  2048 * 3 /44100 = 139.311ms
	periodSize  : DEFAULT_OUT_PERIODSIZE,// size = 2048 * 16/8 * 2 = 8192 byte
    bufferSize  : DEFAULT_OUT_BUFFERSIZE,//2048*3  periods = 3  size = 8192 * 3 = 24 K byte
    periods     : DEFAULT_OUT_PERIODS,
    modPrivate  : 0,
    next		: &_Ext_Out,
};


static alsa_handle_t _defaultsIn = {
    module      : 0,
    devices     : AudioSystem::DEVICE_IN_ALL,
    curDev      : 0,
    curMode     : 0,
    handle      : 0,
    format      : SND_PCM_FORMAT_S16_LE, // AudioSystem::PCM_16_BIT
    channels    : 1,
    sampleRate  : 0,//android::AudioRecord::DEFAULT_SAMPLE_RATE,//note:we do not use this samplerate.added by  zxg
    latency     : DEFAULT_IN_LATENCY, // Desired Delay in usec
	periodSize  : DEFAULT_IN_PERIODSIZE,
    bufferSize  : DEFAULT_IN_BUFFERSIZE, // Desired Number of samples
    periods     : DEFAULT_IN_PERIODS,	
    modPrivate  : 0,
    next		: 0,
};


struct device_suffix_t {
    const AudioSystem::audio_devices device;
    const char *suffix;
};

/* The following table(s) need to match in order of the route bits
 */
static const device_suffix_t deviceSuffix[] = {
        {AudioSystem::DEVICE_OUT_EARPIECE,       "_Earpiece"},
        {AudioSystem::DEVICE_OUT_SPEAKER,        "_Speaker"},
        {AudioSystem::DEVICE_OUT_WIRED_HEADSET,  "_Headset"},
        {AudioSystem::DEVICE_OUT_WIRED_HEADPHONE,  "_Headphone"},
        {AudioSystem::DEVICE_OUT_BLUETOOTH_SCO,  "_Bluetooth"},
        {AudioSystem::DEVICE_OUT_BLUETOOTH_SCO_HEADSET,  "_Bluetooth"},
        {AudioSystem::DEVICE_OUT_BLUETOOTH_A2DP, "_Bluetooth-A2DP"},
        {AudioSystem::DEVICE_OUT_AUX_DIGITAL, "_Aux"},
        {AudioSystem::DEVICE_OUT_ANLG_DOCK_HEADSET, "_Spdif"},
        {AudioSystem::DEVICE_OUT_DGTL_DOCK_HEADSET, "_Usbaudio"},
        /*device in*/    
        {AudioSystem::DEVICE_IN_DGTL_DOCK_HEADSET, "_Usbaudio"},
};

static const int deviceSuffixLen = (sizeof(deviceSuffix)
        / sizeof(device_suffix_t));

// ----------------------------------------------------------------------------

snd_pcm_stream_t direction(alsa_handle_t *handle)
{
    return (handle->devices & AudioSystem::DEVICE_OUT_ALL) ? SND_PCM_STREAM_PLAYBACK
            : SND_PCM_STREAM_CAPTURE;
}

/**
 * Convert an integer, positive or negative, to a character string radix 10.
 */
char* itoa(int32_t i, char* buf)
{
    char* result = buf;

    // Handle negative
    if (i < 0)
    {
        *buf++ = '-';
        i = -i;
    }

    // Output digits in reverse order
    char* p = buf;
    do
    {
        *p++ = (char)('0' + (i % 10));
        i /= 10;
    }
    while (i);
    *p-- = 0;

    // Reverse the string
    while (buf < p)
    {
        char c = *buf;
        *buf++ = *p;
        *p-- = c;
    }

    return result;
}


const char *deviceName(alsa_handle_t *handle, uint32_t device, int mode)
{
	static char devString_capture[ALSA_NAME_MAX];
	static char devString_playback[ALSA_NAME_MAX];
    char *devString;
    int hasDevExt = 0;

	if(direction(handle) == SND_PCM_STREAM_CAPTURE)
		devString = devString_capture;
	else
		devString = devString_playback;
	
    strcpy(devString, devicePrefix[direction(handle)]);
    if(device == AudioSystem::DEVICE_OUT_DGTL_DOCK_HEADSET ||
        device == AudioSystem::DEVICE_IN_DGTL_DOCK_HEADSET){

		ALSA_STRCAT (devString, "_Usbaudio_");
		char buf[32];
		char *ccard = (direction(handle) == SND_PCM_STREAM_CAPTURE)? itoa(usb_capture_card, buf): itoa(usb_playback_card, buf);
		ALSA_STRCAT (devString, ccard);
		ALSA_STRCAT (devString, "_normal");		
		
	}else{

	    for (int dev = 0; device && dev < deviceSuffixLen; dev++)
	        if (device & deviceSuffix[dev].device) {
	            ALSA_STRCAT (devString, deviceSuffix[dev].suffix);
	            device &= ~deviceSuffix[dev].device;
	            hasDevExt = 1;
	        }

	    if (hasDevExt) switch (mode) {
	    case AudioSystem::MODE_NORMAL:
	        ALSA_STRCAT (devString, "_normal")
	        ;
	        break;
	    case AudioSystem::MODE_RINGTONE:
	        ALSA_STRCAT (devString, "_ringtone")
	        ;
	        break;
	    case AudioSystem::MODE_IN_CALL:
	        ALSA_STRCAT (devString, "_incall")
	        ;
	        break;
	    case AudioSystem::MODE_IN_COMMUNICATION:
	        ALSA_STRCAT (devString, "_voip")
	        ;
	        break;		
		default:
			LOGW("Error on strcat alsa_device_name");
			break;
	    };
	}
	
    return devString;
}

const char *streamName(alsa_handle_t *handle)
{
    return snd_pcm_stream_name(direction(handle));
}

static void resetHardwareParams(alsa_handle_t *handle, int flag) {
    snd_pcm_uframes_t bufferSize;

    if(direction(handle) == SND_PCM_STREAM_PLAYBACK) {

        handle->latency = DEFAULT_OUT_LATENCY;
        handle->periods = DEFAULT_OUT_PERIODS;

        if(8 == handle->channels && 1==flag){
            handle->periodSize = 1024*8;
        }else if(2 == handle->channels && 192000 == handle->sampleRate){
            handle->periodSize = 1024*8;
        }else{
            handle->periodSize = DEFAULT_OUT_PERIODSIZE;
        }

        handle->bufferSize = handle->periodSize * handle->periods;
    } else {
    #if 0
        bufferSize = DEFAULT_IN_BUFFERSIZE;
        for (size_t i = 1; (bufferSize & ~i) != 0; i <<= 1)
            bufferSize &= ~i;
        //handle->bufferSize = bufferSize;
	#else
		//handle->bufferSize = DEFAULT_IN_BUFFERSIZE;
	#endif
        handle->latency = DEFAULT_IN_LATENCY;
		//handle->periodSize = DEFAULT_IN_PERIODSIZE;
		handle->periods = DEFAULT_IN_PERIODS;
               
    }    
}

status_t setHardwareParams(alsa_handle_t *handle, int flag)
{
    snd_pcm_hw_params_t *hardwareParams;
    status_t err;
    
    resetHardwareParams(handle, flag);
    
    snd_pcm_uframes_t bufferSize = handle->bufferSize;
	snd_pcm_uframes_t periodSize = handle->periodSize;
    unsigned int requestedRate = handle->sampleRate;
    unsigned int latency = handle->latency;
	unsigned int periods = handle->periods;
	unsigned int periodTime = 0;
	LOGE("buffersize: %d, periodsize:%d\n", bufferSize, periodSize);
    // snd_pcm_format_description() and snd_pcm_format_name() do not perform
    // proper bounds checking.
    bool validFormat = (static_cast<int> (handle->format)
            > SND_PCM_FORMAT_UNKNOWN) && (static_cast<int> (handle->format)
            <= SND_PCM_FORMAT_LAST);
    const char *formatDesc = validFormat ? snd_pcm_format_description(
            handle->format) : "Invalid Format";
    const char *formatName = validFormat ? snd_pcm_format_name(handle->format)
            : "UNKNOWN";

    if (snd_pcm_hw_params_malloc(&hardwareParams) < 0) {
        LOG_ALWAYS_FATAL("Failed to allocate ALSA hardware parameters!");
        return NO_INIT;
    }

    err = snd_pcm_hw_params_any(handle->handle, hardwareParams);
    if (err < 0) {
        LOGE("Unable to configure hardware: %s", snd_strerror(err));
        goto done;
    }

    // Set the interleaved read and write format.
    err = snd_pcm_hw_params_set_access(handle->handle, hardwareParams,
            SND_PCM_ACCESS_RW_INTERLEAVED);
    if (err < 0) {
        LOGE("Unable to configure PCM read/write format: %s",
                snd_strerror(err));
        goto done;
    }

    err = snd_pcm_hw_params_set_format(handle->handle, hardwareParams,
            handle->format);
    if (err < 0) {
        LOGE("Unable to configure PCM format %s (%s): %s",
                formatName, formatDesc, snd_strerror(err));
        goto done;
    }

    LOGV("Set %s PCM format to %s (%s)", streamName(handle), formatName, formatDesc);

    err = snd_pcm_hw_params_set_channels(handle->handle, hardwareParams,
            handle->channels);
    if (err < 0) {
        LOGE("Unable to set channel count to %i: %s",
                handle->channels, snd_strerror(err));
        goto done;
    }

    LOGV("Using %i %s for %s.", handle->channels,
            handle->channels == 1 ? "channel" : "channels", streamName(handle));

    err = snd_pcm_hw_params_set_rate_near(handle->handle, hardwareParams,
            &requestedRate, 0);

    if (err < 0)
        LOGE("Unable to set %s sample rate to %u: %s",
                streamName(handle), handle->sampleRate, snd_strerror(err));
    else if (requestedRate != handle->sampleRate)
        // Some devices have a fixed sample rate, and can not be changed.
        // This may cause resampling problems; i.e. PCM playback will be too
        // slow or fast.
        LOGW("Requested rate (%u HZ) does not match actual rate (%u HZ)",
                handle->sampleRate, requestedRate);
    else
        LOGV("Set %s sample rate to %u HZ", streamName(handle), requestedRate);

#ifdef DISABLE_HARWARE_RESAMPLING
    // Disable hardware re-sampling.
    err = snd_pcm_hw_params_set_rate_resample(handle->handle,
            hardwareParams,
            static_cast<int>(resample));
    if (err < 0) {
        LOGE("Unable to %s hardware resampling: %s",
                resample ? "enable" : "disable",
                snd_strerror(err));
        goto done;
    }
#endif

    // Make sure we have at least the size we originally wanted
    err = snd_pcm_hw_params_set_buffer_size_near(handle->handle, hardwareParams,
            &bufferSize);

    if (err < 0) {
        LOGE("Unable to set buffer size to %d:  %s",
                (int)bufferSize, snd_strerror(err));
        goto done;
    }
	
    err = snd_pcm_hw_params_set_period_size_near(handle->handle, hardwareParams,
            &periodSize,NULL);
    if (err < 0) {
        LOGE("Unable to set buffer size to %d:  %s",
                (int)periodSize, snd_strerror(err));
        goto done;
    }

    // Setup buffers for latency
    err = snd_pcm_hw_params_set_buffer_time_near(handle->handle,
            hardwareParams, &latency, NULL);

    if (err < 0) {
        /* That didn't work, set the period instead */
        unsigned int periodTime = latency / periods;
        err = snd_pcm_hw_params_set_period_time_near(handle->handle,
                hardwareParams, &periodTime, NULL);
        if (err < 0) {
            LOGE("Unable to set the period time for latency: %s", snd_strerror(err));
            goto done;
        }
        
        err = snd_pcm_hw_params_get_period_size(hardwareParams, &periodSize,
                NULL);
        if (err < 0) {
            LOGE("Unable to get the period size for latency: %s", snd_strerror(err));
            goto done;
        }
        bufferSize = periodSize * periods;
        if (bufferSize < handle->bufferSize) bufferSize = handle->bufferSize;
        err = snd_pcm_hw_params_set_buffer_size_near(handle->handle,
                hardwareParams, &bufferSize);
        if (err < 0) {
            LOGE("Unable to set the buffer size for latency: %s", snd_strerror(err));
            goto done;
        }
    } else {
        // OK, we got buffer time near what we expect. See what that did for bufferSize.
        err = snd_pcm_hw_params_get_buffer_size(hardwareParams, &bufferSize);
        if (err < 0) {
            LOGE("Unable to get the buffer size for latency: %s", snd_strerror(err));
            goto done;
        }
        // Does set_buffer_time_near change the passed value? It should.
          
        err = snd_pcm_hw_params_get_buffer_time(hardwareParams, &latency, NULL);
        if (err < 0) {
            LOGE("Unable to get the buffer time for latency: %s", snd_strerror(err));
            goto done;
        }
	
        periodTime = latency/periods;		
        err = snd_pcm_hw_params_set_period_time_near(handle->handle,
                hardwareParams, &periodTime, NULL);
        if (err < 0) {
            LOGE("Unable to set the period time for latency: %s", snd_strerror(err));
            goto done;
        }
    }	

    LOGE("audio type flag: %d\n", flag);
    err = snd_pcm_hw_params_set_flags(handle->handle, hardwareParams, flag);
    if(err<0)
        LOGE("snd_pcm_hw_params_set_flags fail.");

    LOGV("Buffer size: %d(frames)  period Size: %d(frames)  periods: %d (buffersize=periodSize*periods)", (int)bufferSize,(int)periodSize,periods);
    LOGV("Latency: %d   periodTime: %d", (int)latency,periodTime);

    handle->bufferSize = bufferSize;
    handle->latency = latency;
    handle->periodSize = periodSize;
    // Commit the hardware parameters back to the device.
    err = snd_pcm_hw_params(handle->handle, hardwareParams);
    if (err < 0) LOGE("Unable to set hardware parameters: %s", snd_strerror(err));
	
    done:
    snd_pcm_hw_params_free(hardwareParams);
    dumpHandleInfo(handle);
    return err;
}

status_t setSoftwareParams(alsa_handle_t *handle)
{
    snd_pcm_sw_params_t * softwareParams;
    int err;

    snd_pcm_uframes_t bufferSize = 0;
    snd_pcm_uframes_t periodSize = 0;
    snd_pcm_uframes_t startThreshold, stopThreshold;

    if (snd_pcm_sw_params_malloc(&softwareParams) < 0) {
        LOG_ALWAYS_FATAL("Failed to allocate ALSA software parameters!");
        return NO_INIT;
    }

    // Get the current software parameters
    err = snd_pcm_sw_params_current(handle->handle, softwareParams);
    if (err < 0) {
        LOGE("Unable to get software parameters: %s", snd_strerror(err));
        goto done;
    }

    // Configure ALSA to start the transfer when the buffer is almost full.
    snd_pcm_get_params(handle->handle, &bufferSize, &periodSize);

    if (handle->devices & AudioSystem::DEVICE_OUT_ALL) {
        // For playback, configure ALSA to start the transfer when the
        // buffer is full.
        startThreshold = bufferSize - 1;
        stopThreshold = bufferSize;
    } else {
        // For recording, configure ALSA to start the transfer on the
        // first frame.
        startThreshold = 1;
        stopThreshold = bufferSize;
    }

    err = snd_pcm_sw_params_set_start_threshold(handle->handle, softwareParams,
            startThreshold);
    if (err < 0) {
        LOGE("Unable to set start threshold to %lu frames: %s",
                startThreshold, snd_strerror(err));
        goto done;
    }

    err = snd_pcm_sw_params_set_stop_threshold(handle->handle, softwareParams,
            stopThreshold);
    if (err < 0) {
        LOGE("Unable to set stop threshold to %lu frames: %s",
                stopThreshold, snd_strerror(err));
        goto done;
    }

    // Allow the transfer to start when at least periodSize samples can be
    // processed.
    err = snd_pcm_sw_params_set_avail_min(handle->handle, softwareParams,
            periodSize);
    if (err < 0) {
        LOGE("Unable to configure available minimum to %lu: %s",
                periodSize, snd_strerror(err));
        goto done;
    }
	
    // Commit the software parameters back to the device.
    err = snd_pcm_sw_params(handle->handle, softwareParams);
    if (err < 0) LOGE("Unable to configure software parameters: %s",
            snd_strerror(err));

    done:
    snd_pcm_sw_params_free(softwareParams);

    return err;
}

// ----------------------------------------------------------------------------

static status_t s_init(alsa_device_t *module, ALSAHandleList &list)
{
    list.clear();

    snd_pcm_uframes_t bufferSize = _defaultsOut.bufferSize;

    for (size_t i = 1; (bufferSize & ~i) != 0; i <<= 1)
        bufferSize &= ~i;
    _defaultsOut.module = module;
//    _defaultsOut.bufferSize = bufferSize;
	list.push_back(_defaultsOut);

    bufferSize = _defaultsIn.bufferSize;
    for (size_t i = 1; (bufferSize & ~i) != 0; i <<= 1)
        bufferSize &= ~i;
    _defaultsIn.module = module;
//    _defaultsIn.bufferSize = bufferSize;
    list.push_back(_defaultsIn);

     bufferSize = _Ext_Out.bufferSize;
	 for (size_t i = 1; (bufferSize & ~i) != 0; i <<= 1)
		 bufferSize &= ~i;
	 _Ext_Out.module = module;
//    _Ext_Out.bufferSize = bufferSize;
	//do not push to list. the only entry is _defaultsOut->next

    return NO_ERROR;
}
static status_t s_open_l(alsa_handle_t *handle, uint32_t devices, int mode, int flag)
{
	pthread_mutex_lock(&lock);
    // Add by ChenJQ, when record over or set channel, 
    // system will set capture channel with device 0,
    // return it immediately.
    if ((direction(handle) == SND_PCM_STREAM_CAPTURE && devices == 0) 
		|| (devices == 0 && mode == 0)) {
		LOGD("direction(handle) == SND_PCM_STREAM_CAPTURE && devices == 0 or devices == 0 && mode == 0");
		pthread_mutex_unlock(&lock);
        return NO_ERROR;
    }

    LOGV("SAMPLERATE:%d\n", handle->sampleRate);
    // Close off previously opened device.
    // It would be nice to determine if the underlying device actually
    // changes, but we might be recovering from an error or manipulating
    // mixer settings (see asound.conf).	
    // capture not standby,so close .
    if(handle->handle || direction(handle)==SND_PCM_STREAM_CAPTURE)
    	s_close_l(handle);
	
    LOGD("open called for devices %08x in mode %d...", devices, mode);

    const char *stream = streamName(handle);
    const char *devName = deviceName(handle, devices, mode);
	LOGD("DevName: %s", devName);

    int err;
	int err_count=10;
    for (;;) {
        // The PCM stream is opened in blocking mode, per ALSA defaults.  The
        // AudioFlinger seems to assume blocking mode too, so asynchronous mode
        // should not be used.
        err = snd_pcm_open(&handle->handle, devName, direction(handle),
                SND_PCM_ASYNC);		
		//add by qjb
		if(err == -EBUSY)
		{
			LOGW("snd_pcm_open EBUSY count = %d , will try again.......",err_count);
			err_count -- ;
			usleep(1000 * 100);
			if (err_count == 0) break;
			continue;
		}

        if (err == 0) break;

        // See if there is a less specific name we can try.
        // Note: We are changing the contents of a const char * here.
        char *tail = strrchr(devName, '_');
        if (!tail) break;
        *tail = 0;
    }

    if (err < 0) {
        // None of the Android defined audio devices exist. Open a generic one.
        LOGE("Using default pcm");
        devName = "default";
        err = snd_pcm_open(&handle->handle, devName, direction(handle), 0);
    }

    if (err < 0) {
        LOGE("Failed to Initialize any ALSA %s device: %s",
                stream, strerror(err));
		pthread_mutex_unlock(&lock);
        return NO_INIT;
    }

    err = setHardwareParams(handle, flag);

    if(err < 0) {
        LOGE("Failed to Initialize any ALSA %s device: %s",
                stream, strerror(err));        
        s_close_l(handle);
        pthread_mutex_unlock(&lock);
        return NO_INIT;
    }    

    if (err == NO_ERROR) err = setSoftwareParams(handle);

    //LOGI("Initialized ALSA %s device %s", stream, devName);

    handle->curDev = devices;
    handle->curMode = mode;
	pthread_mutex_unlock(&lock);
    return err;
}

static status_t s_open(alsa_handle_t *handle, uint32_t devices, int mode, int flag){
	int err;
	struct alsa_handle_t *pHandle = handle->next;
	char value[PROPERTY_VALUE_MAX] = "";	
	uint32_t device_out_soc = (AudioSystem::DEVICE_OUT_SPEAKER|AudioSystem::DEVICE_OUT_AUX_DIGITAL|
								AudioSystem::DEVICE_OUT_WIRED_HEADSET|AudioSystem::DEVICE_OUT_WIRED_HEADPHONE|
								AudioSystem::DEVICE_OUT_EARPIECE);
	s_close(handle);
	//LOGI("open devices: 0x%x\n", devices);

    if(devices == (AudioSystem::DEVICE_OUT_AUX_DIGITAL|AudioSystem::DEVICE_OUT_ANLG_DOCK_HEADSET)) {
		err = s_open_l(handle, AudioSystem::DEVICE_OUT_AUX_DIGITAL, mode, flag);
		if(err < 0)
			return err;
		pHandle->sampleRate = handle->sampleRate;
		return s_open_l(pHandle, AudioSystem::DEVICE_OUT_ANLG_DOCK_HEADSET, mode, flag);		
	}else{
		return s_open_l(handle, devices, mode, flag);
	}
}

static status_t s_close_l(alsa_handle_t *handle){
	
    status_t err = NO_ERROR;
    snd_pcm_t *h = handle->handle;
    handle->handle = 0;
    handle->curDev = 0;
    handle->curMode = 0;
    if (h) {
        //snd_pcm_drain(h);
        //LOGE("do not drain pcm!");
        err = snd_pcm_close(h);
    }
	return err;

}

static status_t s_close(alsa_handle_t *handle)
{
	//LOGD("ALSA s_close....");
    status_t err = NO_ERROR;
	struct alsa_handle_t *pHandle = handle;
	do {
		err = s_close_l(pHandle);
		if(err < 0)
			LOGE("%s: %s", __FUNCTION__, strerror(err));
		pHandle = pHandle->next;
	} while(pHandle);
	usleep(1000 * 100);//add by qjb 
    return err;
}

static status_t s_standby_l(alsa_handle_t *handle)
{
//	LOGD("ALSA s_standby  handle->handle = %d ,handle->handle_call = %d",handle->handle,handle->handle_call);	
    status_t err = NO_ERROR;
    snd_pcm_t *h = handle->handle;	

    if (h) {
        //snd_pcm_drain(h);
        //LOGE("do not drain pcm!");
        err = snd_pcm_close(h);
    }

	//usleep(1000 * 100);//add by qjb
	handle->handle = 0;
    return err;
}

static status_t s_standby(alsa_handle_t *handle){
    status_t err = NO_ERROR;
	struct alsa_handle_t *pHandle = handle;
	do {
		err = s_standby_l(pHandle);
		if(err < 0)
			LOGE("%s: %s", __FUNCTION__, strerror(err));
		pHandle = pHandle->next;
	} while(pHandle);
	usleep(1000 * 10);//add by qjb 
    return err;

}

static status_t s_route(alsa_handle_t *handle, uint32_t devices, int mode, int flag)
{
	pthread_mutex_lock(&lock);
    LOGD("route called for devices %08x in mode %d...", devices, mode);
	
	//usb audio: passthrough
	if(devices == AudioSystem::DEVICE_OUT_DGTL_DOCK_HEADSET||
		devices == AudioSystem::DEVICE_IN_DGTL_DOCK_HEADSET);	
    else if (devices==0 ||(handle->handle && handle->curDev == devices && handle->curMode == mode)) 
	{
		LOGD("s_route Audio is working in the device %s",deviceName(handle, devices, mode));
		pthread_mutex_unlock(&lock); 
		return NO_ERROR;
	}
	pthread_mutex_unlock(&lock);
    return s_open(handle, devices, mode, flag);
}




static status_t s_set_usb_device(int streamType, uint32_t card, uint32_t device)
{

	pthread_mutex_lock(&lock);
    LOGD("s_set_usb_device: streamType %d; card=%d;device=%d", streamType, card, device);

	if(card<0 || device<0){
		LOGE("invalid card or device.");
		pthread_mutex_unlock(&lock);
		return BAD_VALUE;
	}

    if (streamType == SND_PCM_STREAM_CAPTURE) {
		usb_capture_card = card;
		usb_capture_device = device;
    } else {
		usb_playback_card = card;
		usb_playback_device = device;
	}
	pthread_mutex_unlock(&lock);
	return 0;
}


}
